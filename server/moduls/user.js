//ייבוא החבילה של מונגוס
const mongoose = require('mongoose')
// יצירת משתנים מתוך מונגוס
//מחלקה שממנה ניצור אובייקט שיכיל את הסכמה Schema
const {Schema, model} = mongoose
//יצירת סכמה חדשה
const userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    
    password:{
        type: String,
        required: true,
        select: false
    },
    isActive: {
        type: Boolean,
        default: true
    },
    lastSeen: {
        type: Date,
        default: Date.now
    },
    token: String
})
//יצירת מודל חדש(המשנים: שם המודול כפי שיקבע במסד הנתונים, והסכמה שמגדירה אותו)
const UserModel = model('user', userSchema)
//ייצוא של המודל כדי שנוכל להשתמש בו במקומות אחרים
module.exports = UserModel