const { text } = require('express')
const mongoose = require('mongoose')
const { Schema, model } = mongoose

const testSchema = new Schema({
    name: {
        type: String
    },
    description: {
        type: String
    },
    creationDate: {
        type: Date,
        default: Date.now
    },
    creatorId: {
        type: String
    },
    deadline: {
        type: Date
    },
    active: {
        type: Boolean,
        default: true
    },
    type: {
        type: String,
        enum: ["Test", "Survey"],
        default: "Test"
    },
    status: {
        type: String,
        enum: ["Draft", "Create", "Done"],
        default: "Draft"

    },
    shared: {
        type: Boolean,
        default: false
    },
    entryFromLink: {
        type: Number,
        default: 0
    }
    

})

const TestModel = model('test', testSchema)

module.exports = TestModel