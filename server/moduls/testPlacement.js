const mongoose = require('mongoose')

const { Schema, model } = mongoose


const testPlacementSchema = new Schema({

    examineeEmail: {
        type: String,
        unique: false
    },
    testId: {
        type: String,
        unique: false


    },
    grade: {
        type: Number
    },
    status: {
        type: String,
        enum: ["Assigned", "Done"],
        default: 'Assigned'
    },
    dateOfSubmission: {
        type: Date,
        default: Date.now
    },
    dateOfStart: {
        type: Date
    },
    answers: [
        {
            questionId: String,
            answersId: [String]
        }
    ]
})
//יחודי testId -ו examineeId מגדיר שהצירוף של השדות
testPlacementSchema.index({ examineeEmail: 1, testId: 1 }, { unique: true })

const ModelTestPlacement = model('test_Placement', testPlacementSchema)

module.exports = ModelTestPlacement


