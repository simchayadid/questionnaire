const mongoose = require('mongoose')
const { Schema, model } = mongoose

const questionSchema = new Schema({
    context: {
        type: String
    },
    Description: {
        type: String,
        // required: true
    },
    type: {
        type: String,
        enum: ["oneAnser", "manyAnsers","yes/no"]
    },
    testId: {
        type: String
    },
    required:{
       type: Boolean,
       default: true
    },
    points: {
        type: Number,
        default: 0
    },
    active: {
        type: Boolean,
        default: true
    },
    answers: [
        {
            text: String,
            isCorrect: Boolean,
            isActive : Boolean
        }
    ]
})

const ModelQuestion = model('question', questionSchema)

module.exports = ModelQuestion

