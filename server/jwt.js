//עוזרת לנו ליצור טוקן  jsonwebtoken החבילה 
//(npm התקנתי את החבילה מ)
const jwt = require('jsonwebtoken')


//some key to open the token. should be in ".env" file
const praivetKey = '34534h5jhj65' //process.env.SECRET
//פונקציה שמקבלת מזהה ומחזירה טוקן
exports.createToken = (id) => {
    const token = jwt.sign({ id }, praivetKey, { expiresIn: '30d' })

    return token
}
//פונקציה שמקבלת טוקן ומחזירה מזהה משתמש
exports.verifyToken = (token) => {
    const tokenData = jwt.verify(token, praivetKey) || {}

    if (!tokenData.id || Date.now() > tokenData.exp * 1000)
        throw { error: 'token unouthorized' }

    return tokenData.id
}


// const ttt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxMTQyZjcyYzI2MTUyMzU3OGRkYzQwOSIsImlhdCI6MTYyODgwMjM4MCwiZXhwIjoxNjMxMzk0MzgwfQ.z3UO6ZSK_9PWk6C2DWCSZsZ3PgMQf3NYNtJPuD5eeTg'
// debugger
// verifyToken(ttt)
