// ייבוא של הסכמה כדי שנוכל להשתמש במבנה של הסכמה
const User = require('../moduls/user')
// יבוא של החבילה שמצפינה את הסיסמא
const bcryptjs = require('bcryptjs')
//ייבוא של הפונקציות מהקובץ שמטפל בטוקן
const { createToken, verifyToken } = require('../jwt')



async function create(data) {
    console.log(11);
    return User.create(data)// שימוש בפונקציה פנימית של מונגוס
}
exports.create = create

async function read(filter) {
    filter.isActiv = true


    return User.find(filter)
}
exports.read = read

async function readOne(filter, projection) {
    console.log(88);
    filter.isActive = true

    return User.findOne(filter, projection)
        .then(result =>
            result)
        .catch(() =>
            null)
}
exports.readOne = readOne



async function update(_id, newData) {
    // אומר שמה שיעודכן יהיה המידע החדש { new: true } הפרמטר
    return User.findByIdAndUpdate(_id, newData, { new: true })
}
exports.update = update


exports.register = async function register(data) {
    console.log(data);
    if (!data.firstName)
        throw `'firstName' is required`

    if (!data.lastName)
        throw `'lastName' is required`
    if (data.password != data.password2)
        throw 'the passwords not match'
    console.log(99);
    data.name = `${data.firstName} ${data.lastName}`
    //הצפנה של הסיסמה
    data.password = bcryptjs.hashSync(data.password)
    console.log(data);
    return await create(data)

}


exports.login = async function login(email, password) {

    const user = await readOne({ email }, '+password')
    console.log(11111111);
    console.log(user);

    if (!user) throw 'failed to login'

    if (!bcryptjs.compareSync(password, user.password))
        throw 'failed to login'
    const token = createToken(user._id)
    console.log(token);

    user.token = token

    user.lastSeen = Date.now()
    console.log(user.lastSeen);
    console.log(user);
    return await update(user._id, user)
}
//התחברות למערכת באמצעות טוקן
exports.loginByToken = async function loginByToken(token) {
    console.log(77);
    console.log(token);
    const user = await getUserByToken(token)
    user.lastSeen = Date.now()
    const updatedUser = await update(user._id, user)
    console.log(updatedUser);

    return updatedUser
}
exports.logout = async function logout(token) {
    const user = await getUserByToken(token)
    console.log(user);
    user.token = ""
    console.log(user);
    return await update(user._id, user)
}
//פונקציה שמקבלת טוקן ומחזירה את המשתמש
async function getUserByToken(token) {
    // אפשרות נוספת למציאת האדם,
    // בגלל שקראתי למשתנה בדיוק בשם שמוגדר במסד הנתונים אני עושה חיפוש לפי המשתנה
    // const _id = verifyToken(token)
    // const user = await readOne({ _id })
    console.log(11111);
    const userId = verifyToken(token)
    console.log(userId);
    const user = await readOne({ _id: userId })
    console.log(99);
    console.log(user);
    console.log(999);
    if (!user) throw 'user does not exist!!!'
    return user
}
exports.getUserByToken = getUserByToken

