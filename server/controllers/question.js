const Question = require('../moduls/question')

async function create(data) {
    console.log(data);
    return Question.create(data)
}
exports.create = create

async function read(filter) {
    filter.active = true
    return Question.find(filter)
}
exports.read = read

async function readOne(filter) {
    filter.active = true
    return Question.findOne(filter)
        .then(result =>
            result)
        .catch(() =>
            null)
}
exports.readOne = readOne

async function update(_id, newData) {
    return Question.findByIdAndUpdate(_id, newData, { new: true })
}
exports.update = update


//פונקציה שמוסיפה תשובה לשאלה
exports.addAnswer = async function addAnswer(idQ) {
    console.log(8888888888888);
    console.log(idQ);
    const question = await readOne({ _id: idQ })
    const newAnswers = question.answers
    const answer = {
        text: "",
        isCorrect: false,
        isActive: true
    }
    newAnswers.push(answer)
    question.answers = newAnswers
    console.log(question);
    return await update(question._id, question)
}
//פונקציה שמעדכנת שאלה
exports.updateQues = async function updateQues(details) {
    console.log(777);
    console.log(details);
    const { name, value, quesId } = details
    console.log(name, value, quesId);
    if (!name || !quesId) throw 'wromg'
    const ques = await readOne({ _id: quesId })
    ques[name] = value
    console.log(ques);
    return await update(ques._id, ques)
}
//פונקציה שמעדכנת תשובה
exports.updateAnswer = async function updateAnswer(details) {
    console.log(details);
    const { index, name, value, quesId } = details
    if (index < 0 || !name || !quesId) throw 'wromg'
    console.log(88);
    const ques = await readOne({ _id: quesId })
    if (name == 'isCorrect')
        ques.answers.forEach(ans => ans.isCorrect = false)
    ques.answers[index][name] = value
    return await update(ques._id, ques)
}

exports.dealetAnswer = async function dealetAnswer(details) {
    console.log(details);
    const { index, quesId } = details
    if ( index < 0 || !quesId) throw 'wromg'

    const ques = await readOne({ _id: quesId })
    ques.answers.splice(index, 1)
    return await update(ques._id, ques)
}

