
const User = require('./user')
const Question = require('./question')
const Test = require('./test')
const TestPlacement = require('./testPlacement')
const Jwt = require('../jwt')
const nodemailer = require("nodemailer");




// // manage token situation
// async function checkToken(token) {
//     try {

//         if (!token) throw { error: 'Token is required' }
//         let verToken = Jwt.verifyToken(token)

//         if (!verToken) throw { error: 'Token is incorrect' }

//         // Convert the token to id:
//         return verToken.id
//     }
//     catch {
//         throw err
//     }
// }
// הוספת מבחן חדש כשהמשתמש לוחץ צור מבחן
exports.createTest = async function createTest(token, name) {
    // const creator_id = await verifyToken(token)
    // const user2 = await User.readOne({ _id: creator_id })
    const userId = await Jwt.verifyToken(token)
    console.log(userId)
    const user = await User.readOne({ _id: userId })
    console.log(user);
    if (!user) throw 'user does not exist!!!'
    let d = new Date()
    d.setMonth(d.getMonth() + 1)
    let test = {
        name: name,
        creationDate: Date.now(),
        deadline: d,
        creatorId: userId,
        active: true,
        type: 'Test',
        status: 'Draft',
        shared: false
    }

    return await Test.create(test);
}
//פונקציה שיוצרת שאלה
exports.creatQues = async function creatQues(testId) {
    console.log(testId);
    const ques = {
        context: '',
        testId: testId,
        answers: [{
            text: '',
            isCorrect: false,
            isActive: true
        }]
    }
    return await Question.create(ques)
}





//פונקציה שבודקת האם מבחן שייך למשתמש שמחובר
async function isTestBelongUser(token, testId) {
    console.log(8888888888888888);

    const creatorId = await creatorOfTest(testId)
    if (!creatorId) throw 'no update'
    console.log(creatorId);
    const userId = await Jwt.verifyToken(token)
    if (!creatorId == userId) throw 'no update'
    return true

}
exports.isTestBelongUser = isTestBelongUser

//פונקציה שמקבלת מזהה מבחן ומחזירה יוצר של מבחן
async function creatorOfTest(_id) {
    console.log(66666);
    console.log(_id);
    const test = await Test.readOne({ _id })
    if (!test) throw 'no update'
    console.log(test.creatorId);
    return test.creatorId
}
exports.creatorOfTest = creatorOfTest

//פונקציה שמחזירה את כל המבחנים שהמשתמש יצר
exports.getTestsThatUserCreat = async function getTestsThatUserCreat(token) {
    console.log(444);
    const userId = await Jwt.verifyToken(token)
    let tests = await Test.read({ creatorId: userId })
    // tests = tests.filter(test => test.active)
    console.log(tests);
    return tests
}


//מחיקת מבחן
exports.dealetTest = async function dealetTest(testID, token) {
    console.log(999999999999999);
    console.log(testID);
    await isTestBelongUser(token, testID)
    const test = await Test.readOne({ _id: testID })
    test.active = false
    console.log(test);
    return await Test.update(test._id, test)
}

//פונקציה שמחזירה את כל המבחנים שהוקצו למשתמש
exports.getTestsOfUser = async function getTestsOfUser(token) {
    console.log(4444444444444444);
    console.log(token);
    // const userId = verifyToken(token)

    const user = await User.getUserByToken(token)
    console.log(44);
    console.log(user.email);
    const tests = await TestPlacement.read({ examineeEmail: user.email })
    console.log(tests);
    return tests

}

//החזרת כל הפרטים של מבחן שהוקצה כולל שאלות ותשובות
exports.getTestThatAssigned = async function getTestThatAssigned(_id) {
    const testPlac = await TestPlacement.readOne({ _id })
    console.log(1);
    const test = await Test.readOne({ _id: testPlac.testId })
    console.log(2);
    const questions = await getQustions(test._id)
    console.log(3);
    const testDetails = [testPlac, test, questions]

    // console.log(questions)
    return testDetails
}
//החזרת השאלות ללא מידע מי התשובה הנכונה
async function getQustions(testId) {
    let questions = await Question.read({ testId })
    console.log(questions);
    questions.forEach(question => {
        question.answers.forEach(answer => {
            //דרך נוספת למחוק מאפיין
            // delete answer._doc.isCorrect
            answer.isActive = undefined
            answer.isCorrect = undefined
        })
    })
    return questions
}
//שמירת מבחן
exports.saveTest = async function saveTest(_id) {
    const test = await Test.readOne({ _id })
    console.log(test);
    if (!test) throw 'failed'
    const questions = await Question.read({ testId: _id })
    console.log(questions);
    if (questions == 0) throw 'At least one question must be created'
    await isScoreOK(questions)
    test.status = "Create"
    await Test.update(_id, test)
    return true
}
async function isScoreOK(questions) {
    console.log(55);
    console.log(questions);
    const totalPoints = questions.reduce((total, q) =>
        total + q.points
        , 0)
    console.log(totalPoints);
    if (totalPoints != 100) throw 'not ok points, plase check agian'
    return true
}
//הקצאת מבחנים ושליחת הודעה במייל
exports.assignAndSandTest = async function assignAndSandTest(data) {
    console.log(9898);
    const emailAddresses = data.email.split(",")
    console.log(emailAddresses);
    emailAddresses.forEach(email => {
        assignTest(email, data.testId)
            .then(() =>
                sendEmail(email)
            )
            .catch(err => {
                console.log(err)
                //איך לזרוק את השגיאה מפה
            })
    })
    return true



}
//הקצאת מבחן למשתמש
async function assignTest(examineeEmail, testId) {
    let answers = []
    const questions = await Question.read({ testId })
    console.log(questions);
    questions.forEach(q => {
        console.log(q._id);
        const answer = {
            questionId: q._id,
            answersId: []
        }
        answers.push(answer)
    })
    const data = {
        examineeEmail: examineeEmail,
        testId: testId,
        answers: answers
    }
    const testPlac = await TestPlacement.create(data)
    if (!testPlac) throw `no assign test ${data.examineeEmail}`
    return true
}
exports.assignTest = assignTest


//שליחת מייל
async function sendEmail(data) {
    //כולל מייל לשליחה, נושא וטקסט data
    console.log(data);
    //הגדרה של המייל השולח
    const transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: 'simchonet@gmail.com',
            pass: 'S9363023'
        }
    })

    // send mail with defined transport object
    await transporter.sendMail({
        from: '" sim@1234 👻" <foo@example.com>', // sender address
        to: data, // list of receivers
        subject: "123", // Subject line
        text: "456"// plain text body
    })
    console.log("Message sent");
}

exports.submitTest = async function submitTest(_id) {
    let grade = 0
    const testPlac = await TestPlacement.readOne({ _id })
    const questions = await Question.read({ testId: testPlac.testId })
    questions.forEach(ques => {
        const answer = testPlac.answers.find(ans => ans.questionId == ques._id)
        const corectAnswer = ques.answers.find(ans => ans.isCorrect)
        if (corectAnswer._id == answer.answersId[0])
            grade += ques.points
    })
    testPlac.grade = grade
    testPlac.status = "Done"
    await TestPlacement.update(_id, testPlac)


}
// sendEmail().catch(console.error)
// מחיקה
// שמירת טיוטה
// שינוי פרטים
//  ואלידציה
//  סטטוס
//  עדכון
async function updateTest(data, token) {
    const creator_id = await checkToken(token)
    const user = await User.readOne({ id: creator_id })
    if (!user) throw { error: 'user does not exist!!!' }

}
