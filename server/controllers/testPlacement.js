const { assert } = require('console')
const TestPlacement = require('../moduls/testPlacement')

async function create(data) {
    return TestPlacement.create(data)
    .then(result =>
        result)
    .catch(() =>
        null)

}
exports.create = create

async function read(filter) {
    return TestPlacement.find(filter)
}
exports.read = read

async function readOne(filter) {

    return TestPlacement.findOne(filter)
        .then(result =>
            result)
        .catch(() =>
            null)
}
exports.readOne = readOne

async function update(_id, newData) {
    return TestPlacement.findByIdAndUpdate(_id, newData, { new: true })
    .then(result =>
        result)
    .catch(() =>
        null)

}
exports.update = update

//עדכון תשובה 
exports.updateAnswerPlac = async function updateAnswerPlac(data) {
    console.log(data);
    const testPlac = await readOne({ _id: data.test })
    console.log(testPlac);
    const ques = testPlac.answers.find(answer =>
        answer.questionId == data.quesId)
    if (!ques) throw 'failed'
    console.log(ques);
    //במקרה שנבחרה תשובה
    if (data.state) {
        const isAnswerEx = ques.answersId.some(answer =>
            answer == data.answer)
        console.log(isAnswerEx);
        if (!isAnswerEx)
            ques.answersId.push(data.answer)
        console.log(testPlac.answers);
        update(testPlac._id, testPlac)
        return true
    }
    //במקרה שהוסרה בחירה מהתשובה
    else {
        console.log(9999);
        ques.answersId.forEach((answer, index) => {
            if (answer == data.answer)
                ques.answersId.splice(index, 1)
        })
        console.log(123);
        console.log(testPlac.answers);
        update(testPlac._id, testPlac)

    }
}
//בדיקה האם המשתמש בחר תשובה מסויימת
exports.getCheckedAnswers = async function getCheckedAnswers(data) {
    const test = await readOne({ _id: data.testId })
    const ques = test.answers.find(answer =>
        answer.questionId == data.quesId)
    console.log(123);
    console.log(ques);
    if (!ques) throw 'failed'
    const isSelect = ques.answersId.some(id =>
        id == data.answerId)
    console.log(isSelect);
    return isSelect
}
//בקשה של כל המבחנים שהוקצו ממבחן מסויים
exports.getTestsOfId = async function getTestsOfId(testId){

}



