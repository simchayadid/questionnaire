const express = require('express')


var cors = require('cors')
const app = express()
//db מתוך connect חיבור לפונקציה 
const { connect } = require('./db')
//router חיבור לקובץ 
const Router = require('./router')

//ממחרוזת לגייסון http להפוך את כל מה שמגיע מהבקשת 
app.use(express.json())
//לגלויים למשתמש public להפוך את כל הקבצים שנמצאים בתקיה 
app.use(express.static('public'))
//כדי שנוכל לפנות לדפדפן בכמה פורטים
app.use(cors())

connect()
  .then(() => {

    Router(app)
    app.listen(process.env.PORT || 4000)

  })





