const User = require('./controllers/user')
const Test = require('./controllers/test')
const Questions = require('./controllers/question')
const TestPlacement = require('./controllers/testPlacement')
const Global = require('./controllers/global')
const nodemailer = require("nodemailer")
module.exports = function Router(app) {

    ////////////////////////////////////////////////////////////////////////////
    //בקשות שמטפלות בחשבון המשתמש
    ///////////////////////////////////////////////////////////////////////////
    //התחברות


    app.post('/login', async function (req, res) {
        console.log(888);
        console.log(req.body);
        const { email, password } = req.body
        try {
            const user = await User.login(email, password)
            console.log(user);
            res.send(user)
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ error: err })
        }
    })
    //הרשמה
    app.post('/register', async function (req, res) {

        const data = req.body
        try {
            const user = await User.register(data)
            res.send(user)
        }
        catch (err) {
            if (err.name == "MongoError")
                res.status(400).send({ error: 'a user with this email already exists' })
            else {
                console.log(err);
                res.status(400).send({ error: err })
            }

        }
    })
    //התחברות לפי טוקן
    app.post('/getUser', async function (req, res) {
        console.log(11);
        try {
            console.log(req.body);
            const { token } = req.body
            const user = await User.loginByToken(token)
            res.send(user)
        }
        catch (err) {
            const error = { ...err }
            error.message = 'enter your details'

        }
    })
    app.post('/logout', async function (req, res) {
        try {
            console.log(4444444444);
            const { token } = req.body
            if (!token) throw 'failed'
            await User.logout(token)
            res.send('You have successfully logged out')
        } catch (err) {
            res.status(400).send({ error: 'You have not logged out' })

        }

    })
    ////////////////////////////////////////////////////////////////////////////
    //בקשות שמטפלות במבחנים שנוצרו ע"י משתמש
    ///////////////////////////////////////////////////////////////////////////
    //יצירת מבחן חדש
    app.post('/creatTest', async function (req, res) {
        console.log(77);
        console.log(req.body);
        const { token, name } = req.body
        console.log(token);
        try {
            const test = await Global.createTest(token, name)
            await Global.creatQues(test._id)
            res.send(test._id)
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ erroe: err })
        }
    })
    //יצירת שאלה חדשה
    app.post('/creatQues', async function (req, res) {
        console.log(req.body);
        const { token, testId } = req.body
        try {
            await Global.isTestBelongUser(token, testId)
            ques = await Global.creatQues(testId)
            // const test = await Test.readOne({ _id: testId })
            console.log(ques);
            res.send(ques.testId)
        }
        catch (err) {
            if (err.message != 'questoin not create') {
                console.log(typeof err);
                // const error ={
                //     err:'123'
                // }
                res.status(400).send({ error: 'questoin not create' })
            }

            else
                res.status(400).send({ error: err })

        }
    })

    //בקשת כל המבחנים שהמשתמש יצר
    app.post('/myTests', async function (req, res) {
        console.log(req.body);
        const { token } = req.body
        console.log(token);
        try {
            const tests = await Global.getTestsThatUserCreat(token)
            console.log(tests);
            res.send(tests)
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ error: err.message })
        }

    })


    //בקשה של מבחן מסוים
    app.get('/getTest/:param1', async function (req, res) {
        const { param1 } = req.params
        try {
            const test = await Test.readOne({ _id: param1 })
            console.log(88);
            console.log(test);
            if (!test) throw 'no test found'
            let questions = await Questions.read({ testId: test._id })
            // questions = questions.filter(ques => ques.active)
            console.log(questions);

            res.send([test, questions])
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ error: err })
        }

    })
    //עדכון פרטים במבחן
    app.post('/updateTest', async function (req, res) {
        console.log(7777777777777777777);
        const details = req.body
        console.log(details);
        try {
            const test = await Test.updateTest(details)
            console.log(test);
            res.send(test)
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ error: 'no update' })
        }
    })

    //עדכון פרטים בשאלה
    app.post('/updateQues', async function (req, res) {
        console.log(7777777);
        const details = req.body
        console.log(details);
        try {
            const ques = await Questions.updateQues(details)
            console.log(ques);
            res.send(ques)
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ error: 'question not deleated' })
            // res.status(400).send({message:'err'})


        }

    })

    //עדכון פרטים בתשובה
    app.post('/updateAnswer', async function (req, res) {
        console.log(55);
        const details = req.body
        console.log(details);
        try {
            const ques = await Questions.updateAnswer(details)
            console.log(ques);
            res.send(ques)
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ error: 'question not update' })

        }

    })

    //הוספת תשובה לשאלה
    app.post('/addAnswer', async function (req, res) {
        console.log(999999);
        const { questionId } = req.body
        console.log(questionId);
        try {
            const question = await Questions.addAnswer(questionId)
            res.send(question)
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ error: 'no update' })

        }


    })
    //מחיקת תשובה
    app.post('/dealetAnswer', async function (req, res) {
        const details = req.body
        try {
            const question = await Questions.dealetAnswer(details)
            res.send(question)
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ error: 'no update' })

        }
    })
    //שמירת מבחן שמשתמש יצר
    app.post('/saveTest', async function (req, res) {
        console.log(123456789);
        try {
            const { testId } = req.body
            console.log(testId);
            if (!testId) throw 'failed'
            const reasolte = await Global.saveTest(testId)
            console.log(reasolte);
            res.send('test saved')
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ error: err })

        }
    })
    //מחיקת מבחן
    app.post('/dealetTest', async function (req, res) {

        try {
            const { testID, token } = req.body
            if (!testID || !token) throw 'no update'
            await Global.dealetTest(testID, token)
            const tests = await Global.getTestsThatUserCreat(token)
            console.log(tests);
            res.send(tests)
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ error: 'err' })
        }
    })
    // //שליחה של הודעות ושיבוץ
    // app.post('/sendTestAtEmail', async function (req, res) {
    //     try {
    //         console.log(77);
    //         const data = req.body
    //         if (!data) throw 'no email'
    //         await Global.sendEmail(data)
    //         res.send('email send')
    //     }
    //     catch (err) {
    //         console.log(err);
    //     }
    // })
    ////////////////////////////////////////////////////////////////////////////
    //בקשות שמטפלות בהקצאות למשתמש
    ///////////////////////////////////////////////////////////////////////////
    //בקשת כל המבחנים שהוקצו למשתמש
    app.get('/getTestsOfUser/:token', async function (req, res) {
        console.log(18888811);
        try {
            const { token } = req.params
            console.log(token);
            const tests = await Global.getTestsOfUser(token)
            res.send(tests)
        }
        catch (err) {
            console.log(err.message);
            res.status(400).send({ error: 'no update' })
        }
    })

    //בקשת מבחן שהוקצה לפי מזהה
    app.get('/getTestThatAssigned/:_id', async function (req, res) {
        console.log("pppp1234567");
        try {
            const { _id } = req.params
            let details = await Global.getTestThatAssigned(_id)
            res.send(details)
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ error: 'no update' })

        }
    })
    //עדכון תשובה במבחן שהוקצה
    app.post('/updateAnswerPlac', async function (req, res) {
        console.log(1111);
        try {
            const data = req.body
            if (!data) throw 'failed'
            await TestPlacement.updateAnswerPlac(data)
            res.send("update done")
        } catch (err) {
            console.log(err);
            res.status(400).send({ error: 'failed' })
        }


    })
    //קבלת התשובות שנבחרו עבור שאלה מסוימת
    app.post('/isAnswerChecked', async function (req, res) {
        try {
            const data = req.body
            console.log(data);
            if (!data) throw 'failed'
            const isChecked = await TestPlacement.getCheckedAnswers(data)
            console.log(isChecked);
            res.send(isChecked)
        }
        catch (err) {
            console.log(err);
            res.status(400).send({ error: 'no update' })

        }
    })
    //בקשה של כל המבחנים שהוקצו למבחן מסוים
    app.get('/getTestsOfId/:testId', async function (req, res) {

        console.log(66);
        const { testId } = req.params
        console.log(testId);
        try {
            if (!testId) throw 'no test'
            const tests = await TestPlacement.read({ testId })
            console.log(tests);
            res.send(tests)

        }
        catch (err) {
            console.log(err);
        }
    })
    //הקצאת מבחן
    app.post('/assignAndSandTest', async function (req, res) {
        try {
            console.log(6565);
            console.log(req.body);
            const data = req.body
            if (!data) throw 'failed'
            await Global.assignAndSandTest(data)
            res.send("message sand")
        }
        catch (err) {
            console.log(55);
            console.log(err);
            res.send(err)
        }

    })
    //הגשת מבחן
    app.post('/submitTest', async function (req, res) {
        try {
            console.log("submitTest");
            const { testId } = req.body
            console.log(testId)
            const grate = await Global.submitTest(testId)
        } catch (err) {
            console.log(err);
        }
    })

    app.get('/', function (req, res) {
        res.status(400).send({ error: 'question not deleated' })
        res.send('Hello World')
    })
}
