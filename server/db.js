//עוזרת לנו לנהל את מונגו דיבי  mongoose החבילה 
//(npm התקנתי את החבילה מ)
const mongoose = require('mongoose')


//מכיל את החיבור למסד הנתונים (הטקסט לקחתי מהאתר של מונגו דיבי) connectionString
const connectionString = `mongodb+srv://simcha:S9363023@cluster0.r4foh.mongodb.net/questionnaire?retryWrites=true&w=majority`

exports.connect = async function connect() {
    try {
        console.log('connecting ..')
        await mongoose.connect(connectionString, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true
        },
            // err => { if (err){ throw err} }
        )

        //success
        console.log('Mongo connected!')
    } catch (error) {
        console.error('Not Connected', error.message)
    }
}