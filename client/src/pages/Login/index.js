import axios from "axios"
import { createContext, useState, useEffect } from "react"
import { useHistory } from "react-router"

import './sb-admin-2.min.css'




// אני מייצאת אותו כדי שנוכל להשתמש בו בכל מקום userContext יצירת קונטקסט בשם
export const userContext = createContext()

export default function Login({ children }) {
    //הגדרת משתנים
    const userState = useState(),//אני יוצרת מצב שאותו אח"כ אני אשלח בקונטקסט
        [mode, setMode] = useState('login'),//or 'register'
        [user, setUser] = userState,
        [error, setError] = useState('')
    let history = useHistory()




    useEffect(() => {
        setError('')
    }, [mode])

    useEffect(isUeserLogged, [])

    async function isUeserLogged() {
        var token = sessionStorage.token ? sessionStorage.token : localStorage.token
        console.log(token);
        if (token) {

            try {
                const res = await axios.post('http://localhost:4000/getUser', { token })
                console.log(res.data.token);
                sessionStorage.token = res.data.token
                console.log(res.data);
                setUser(res.data)
            }
            catch (e) {
                // debugger
                setError(e.response?.data.message || e.message)
                console.log(e.response?.data);
                console.log(error);
            }

        }
    }

    async function login(event) {
        //הקומפוננט מוצג מחדש אני רוצה למנוע את זה  rerender בכל פעם שיש 
        event.preventDefault()
        //אני רוצה שכל מי שיתחבר יופנה לדשבורד, אם לא הייתי כותבת ת השורה הזו היה נכנס לדף האחרון שהיה בו
        history.push("/")

        //איסוף הנתונים מהטופס
        const values = Object.values(event.target)
            .reduce((acc, input) => !input.name ? acc : ({
                ...acc,
                [input.name]: input.type == 'checkbox' ? input.checked : input.value
            }), {}
            )
        console.log(values)
        setError('')

        try {

            const res = await axios.post(`http://localhost:4000/${mode}`, values)
            event.target.reset() //איפוס של השדות בטופס

            if (mode == 'login') {
                if (values.stay)
                    localStorage.token = res.data.token
                //שמירה של הטוקן רק לסשן הנוכחי כדי שגם אם המשתמש ירענן הוא ישאר מחובר

                sessionStorage.token = res.data.token

                setUser(res.data)
            }
            else
            setMode('login')
                // setError('You have successfully registered')
        }
        catch (err) {
            debugger
            event.target.reset() //איפוס של השדות בטופס
            setError(err.response?.data?.error || err.message)
            console.log(err.response?.data);
            console.log(error);
        }
        console.log(event.target);
    }



    // להשתמש בקונטקסט(Login כל הקומפוננטות שיהיו בתוך) בתור ספק, מה שיאפשר לכל הילדים שלו userContext הגדרנו את
    //זה מב שאני רוצה לשלוח בקונטקסט value
    return <userContext.Provider value={userState}>
        {
            user ? children :
                <div className=" bg-gradient-primary " >

                    <div class="container" >

                        {/* Outer Row  */}
                        <div class="row  justify-content-center">

                            <div class="col-xl-10 col-lg-12 col-md-9">

                                <div class="card o-hidden border-0 shadow-lg my-5">
                                    <div class="card-body p-0">
                                        {/*  Nested Row within Card Body */}

                                        <div class="row">
                                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                                            <div class="col-lg-6">
                                                <div class="p-5">
                                                    {
                                                        mode == 'register' ?
                                                            <>

                                                                <div class="text-center">
                                                                    <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                                                                </div>
                                                                <form class="user" onSubmit={login}>

                                                                    <div class="form-group row">
                                                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                                                            <input type="text" name="firstName" class="form-control form-control-user" id="exampleFirstName"
                                                                                placeholder="First Name" autoFocus required />
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="text" name="lastName" class="form-control form-control-user" id="exampleLastName"
                                                                                placeholder="Last Name" required />
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="email" name="email" class="form-control form-control-user" id="exampleInputEmail"
                                                                            placeholder="Email Address" required />
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                                                            <input type="password" name="password" class="form-control form-control-user"
                                                                                id="exampleInputPassword" placeholder="Password" required />
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="password" name="password2" class="form-control form-control-user"
                                                                                id="exampleRepeatPassword" placeholder="Repeat Password" required />
                                                                        </div>
                                                                    </div>
                                                                    <input class="btn btn-primary btn-user btn-block" type="submit" value=" Register Account" />


                                                                    <div style={{ color: 'red' }} >{error}</div>


                                                                </form>
                                                            </> :
                                                            <>
                                                                <form class="user" onSubmit={login}>
                                                                    <div class="text-center">
                                                                        <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="email" name="email" class="form-control form-control-user"
                                                                            id="exampleInputEmail" aria-describedby="emailHelp"
                                                                            placeholder="Enter Email Address..." autoFocus required />
                                                                    </div>

                                                                    {/* <input type="email" name="email" placeholder="enter your email" autoFocus required></input> */}

                                                                    <div class="form-group">
                                                                        <input type="password" name="password" class="form-control form-control-user"
                                                                            id="exampleInputPassword" placeholder="Password" required />
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="custom-control custom-checkbox small">
                                                                            <input type="checkbox" class="custom-control-input" id="customCheck" />
                                                                            <label class="custom-control-label text-gray-900" for="customCheck">Remember Me</label>
                                                                        </div>
                                                                    </div>

                                                                    <input class="btn btn-primary btn-user btn-block" type="submit" value="Login" />

                                                                    <div style={{ color: 'red' }} >{error}</div>

                                                                </form>
                                                            </>
                                                    }
                                                    <hr />
                                                    <div class="text-center">
                                                        <a class="small" href="forgot-password.html">Forgot Password?</a>
                                                    </div>
                                                    {mode == 'register' ?
                                                        <div class="text-center">
                                                            <a class="small" onClick={() => setMode('login')}>Already have an account? Login!</a>
                                                        </div> :
                                                        <div class="text-center">
                                                            <a class="small" onClick={() => setMode('register')}>Create an Account!</a>
                                                        </div>}
                                                    {/* <div>
                                                        {
                                                        mode == 'register' ?
                                                            <div> alredy registered?<b onClick={() => setMode('login')}> login</b> </div> :
                                                            <div> not registered?<b onClick={() => setMode('register')}> register</b></div>
                                                    }
                                                    </div> */}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
        }
    </userContext.Provider >
}