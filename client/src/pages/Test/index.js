import axios from "axios"
import { useEffect, useState, createContext } from "react"
import { useParams } from "react-router"
import Loader from "react-loader-spinner";
import QuestionPlac from "../../componnets/QuestionPlac";

export const testContext = createContext()

export default function Test() {

    const { testId, asks } = useParams(),
        [testPlac, setTestPlac] = useState(),
        [test, setTest] = useState(),
        [questions, setQuestions] = useState()
    console.log(333);
    console.log(asks);
    useEffect(async () => {
        try {
            const res = await axios.get(`http://localhost:4000/getTestThatAssigned/${testId}`)
            console.log(res.data);
            // debugger
            setTestPlac(res.data[0])
            setTest(res.data[1])
            setQuestions(res.data[2])
        }
        catch (err) {
            console.log(err);
        }

    }, [])

    async function submitTest(event) {
        event.preventDefault()
        console.log(event.target)
        const res = await axios.post('')
    }
    return !testPlac || !test || !questions ? <Loader type="Puff" color="#00BFFF" height={50} width={50} /> :
        <testContext.Provider value={testId}>
            {
                asks == 'examinee' && testPlac.status == 'Assigned' ?
                    <div>{console.log(testPlac)}
                        <h2>{test.name}</h2>
                        <h4>תאריך אחרון להגשה:{test.deadline.slice(0, 10)}</h4>
                        <form onSubmit={submitTest}>
                            {questions.map((q, i) =>
                                <QuestionPlac key={q._id} ques={q} index={i} status={testPlac.status} />
                            )}
                            <input type="submit" value="הגש מבחן" />
                        </form>
                    </div> :
                    testPlac.status == 'Done' ?
                        <div>{console.log(testPlac)}
                        <p>מבחן הושלם</p> 
                        <h2>{test.name }</h2>
                        <h3>ציון:{testPlac.grade}</h3>
                            {questions.map((q, i) =>
                                <QuestionPlac key={q._id} ques={q} index={i} status={testPlac.status} />
                            )}
                            {asks == 'tester'?
                            <button>שנה ציון</button>:
                        <></>
                            }

                    </div>:

                            <div>הנבחן  ענה למבחן</div>}
        </testContext.Provider>





}