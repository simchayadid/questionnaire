import { useEffect, useState } from "react"
import axios from "axios"
import './testICreated.css'
import Loader from "react-loader-spinner"
import { Link } from "react-router-dom"
import './dataTables.bootstrap4.css'

export default function TestICreated() {

    const [tests, setTests] = useState(),
        [token, setToken] = useState(),
        [error, setError] = useState('')


    //פונקציה שמביאה נתונים מהשרת, מתבצעת רק בטעינה הראשונית של הדף
    useEffect(async () => {
        console.log('נכנסתי לדף');
        // debugger
        const token = sessionStorage.token ? sessionStorage.token : localStorage.token
        setToken(token)
        let data = { token: token }
        try {
            const res = await axios.post('http://localhost:4000/myTests', data)

            if (res.data)
                setTests(res.data)
            else
                setTests(0)
        }
        catch (err) {
            // debugger
            console.log(err);
            setError(err?.response?.data?.error || err.message)
        }

    }, [])
    async function dealetTest(event) {
        event.preventDefault()
        // debugger
        console.log(token);
        const data = {
            testID: event.target.id,
            token: token
        }
        try {
            const res = await axios.post('http://localhost:4000/dealetTest', data)
            setTests(res.data)
        }
        catch (err) {
            console.log(err);
        }


    }

    return <div className="card shadow mb-4">
        <div className="card-header py-3">
            <h4 className="m-0 font-weight-bold text-primary" style={{paddingLeft:'2em', paddingTop:'1em'}}>Test I Created</h4>
            <Link to={`/test-form`} style={{textAlign:'right', display:'inherit'}}><i class="fas fa-plus fa-2x" ></i></Link>
        </div>
        {
            !tests && !error ?
                <div className="flex-box">
                    < Loader type="Circles" color="#00BFFF" height={50} width={50} />
                </div> :
                error ?
                    < div style={{ color: "red" }}>{error}</div> :
                    //אחרת שואלת האם לא קיימים מבחנים ואם לא מחזירה מה שרלוונטי
                    tests == 0 ? <>
                        עדיין לא יצרת מבחן

                    </> :
                        <div className="card-body">

                            <div className="table-responsive">
                                <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                    <div className="row">
                                        <div className="col-sm-12 col-md-6">
                                            <div className="dataTables_length" id="dataTable_length">
                                                <label>Show     <select name="dataTable_length" aria-controls="dataTable" className=" custom-select custom-select-sm form-control form-control-sm">
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>  entrie  
                                                </label>
                                            </div>
                                        </div>
                                        <div className="col-sm-12 col-md-6">
                                            <div id="dataTable_filter" className="dataTables_filter">
                                                <label className="dataTables_filter">Search:
                                                    <input type="search" className=" form-control form-control-sm" placeholder="" aria-controls="dataTable" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <table className=" table table-bordered pp" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>פעיל</th>
                                                <th>סטטוס</th>
                                                <th>שם</th>
                                                <th>תאריך יצירה</th>
                                                <th></th>

                                            </tr>

                                            {tests.map(test =>
                                                // הכתובת שאליה מפנה כל לינק היא טופס מבחן בצירוף פראמס שהוא המזהה של המבחן
                                                // <Link key={test._id} to={`/test-form/${test._id}`} >
                                                <tr >
                                                    <td> <Link key={test._id} to={`/test-form/${test._id}/${test.status}`} ><i class="far fa-file"></i></Link></td>
                                                    <td>{test.active ? 'yes' : 'no'}</td>
                                                    <td>{test.status}</td>
                                                    <td>{test.name}</td>
                                                    <td>{test.creationDate.substr(0, 10)}</td>
                                                    <td><i onClick={dealetTest} id={test._id} class="far fa-trash-alt"></i></td>
                                                </tr>
                                                // </Link>

                                            )}
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th></th>

                                                <th>פעיל</th>
                                                <th>סטטוס</th>
                                                <th>שם</th>
                                                <th>תאריך יצירה</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>


                                        </tbody>
                                    </table>
                                </div>
                                </div>



                                {/* <div className="row">
                        <div className="col-sm-12 col-md-5">
                            <div className="dataTables_info" id="dataTable_info" role="status" aria-live="polite">oooo</div>
                            <div className="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">

                            </div>
                        </div>
                    </div> */}
                                <img src={require('../../img/form-tracking1.png')}></img>
                            </div >
        }
                        </div >

        }


        //Loader כל עוד לא נטענו נתונים מהשרת להציג את 
    //         !tests && !error?
    //             <>
    //                 {/* <Loader type="Puff" color="#00BFFF" height={50} width={50} /> */}
    //                 <Loader type="Circles" color="#00BFFF" height={50} width={50}/>


    //             </> :
    //             error ?
    //                 < div style={{ color: "red" }}>{error}</div> :
    //                 //אחרת שואלת האם לא קיימים מבחנים ואם לא מחזירה מה שרלוונטי
    //                 tests == 0 ? <>
    //                     עדיין לא יצרת מבחן
    //                     <br />
    //                     <button><Link to={`/test-form`}>הוסף מבחן</Link></button>
    //                 </> :
    //                     // במקרה שקיימים מבחנים מציגה מה שרלוונטי
    //                     <>
    //                         <button><Link to={`/test-form`}>הוסף מבחן</Link></button>

    //                         <table>
    //                             <tr>

    //                                 <th>סוג</th>
    //                                 <th>פעיל</th>
    //                                 <th>סטטוס</th>
    //                                 <th>שם</th>
    //                                 <th>תאריך יצירה</th>
    //                             </tr>
    //                             {/* לאחר שיש לי מערך של כל המבחנים שמשתמש יצר, אני יוצרת מערך חדש של שורות בטבלה,
    //                     כל שורה מכילה לינק שמכיל את פרטי המבחן בכלליות וכשלוחצים על המבחן,
    //                     מגיעים לטופס של המבחן הספציפי */}
    //                             {tests.map(test =>
    //                                 // הכתובת שאליה מפנה כל לינק היא טופס מבחן בצירוף פראמס שהוא המזהה של המבחן
    //                                 <Link key={test._id} to={`/test-form/${test._id}`} >
    //                                     <tr >

    //                                         <td>{test.type}</td>
    //                                         <td>{test.active ? 'yes' : 'no'}</td>
    //                                         <td>{test.status}</td>
    //                                         <td>{test.name}</td>
    //                                         <td>{test.creationDate.substr(0, 10)}</td>
    //                                         <button onClick={dealetTest} id={test._id} className="btn btn-danger btn-circle"><i className="far fa-trash-alt"></i></button>
    //                                     </tr>
    //                                 </Link>
    //                             )}
    //                         </table>
    //                     </>
    //     }
    // </div >
