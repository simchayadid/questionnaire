import axios from "axios";
import { useState, useEffect } from "react";
import { useParams } from "react-router"
import Form from "../../componnets/Form"
import Placement from "../../componnets/Placement"

import './TestForm.css'



export default function TestForm({ }) {
    const
        [tab, setTab] = useState("questions"),
        // (TestICreated) שנשלח מהדף שהפנה לדף הזה Params -קבלת ה
        { testId, status } = useParams()
    console.log(status);

    async function changeTab(event) {
        // debugger
        event.preventDefault()
        placement.classList.toggle("active")
        questions.classList.toggle("active")
        setTab(event.target.textContent)
    }

    return <div >
        {status == 'Create' ?
            <>
                <div class="tabbed round skin-peter-river" id="skinable">
                    <ul>
                        <li  onClick={changeTab} id='placement'>placement</li>
                        <li className='active' onClick={changeTab} id='questions'>questions</li>

                    </ul>
                </div>
                {/* <nav className='left-margin plc-nav'>
                    <button className='plc-link' onClick={changeTab}>questions</button>
                    button className='plc-link' onClick={changeTab}>placement</button>
                </nav>  */}
                </> : ''
        }
        {
            tab == "questions" ?

                <Form testId={testId} />
                :
                <Placement testId={testId} />
        }

    </div>
}



{/* <form>
                    <button>הוסף שאלה</button>
                    <input type="text" name="name" placeholder="test name" autoFocus onBlur={saveData}></input><br />
                    <input type="text" name="description" placeholder="description" onBlur={saveData}></input><br />
                    <label>deadline</label>
                    <input type="date" name="deadline" onBlur={saveData} ></input><br />
                </form> */}