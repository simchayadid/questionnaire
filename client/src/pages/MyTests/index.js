import axios from "axios"
import { useEffect, useState } from "react"
import Loader from "react-loader-spinner"
import { Link } from "react-router-dom"


export default function MyTests() {
    const [myTests, setMyTests] = useState()


    useEffect(async () => {
        console.log('נכנסתי לדף');
        const token = sessionStorage.token
        console.log(token);
        try {
            const res = await axios.get(`http://localhost:4000/getTestsOfUser/${token}`)
        console.log(res.data);
        setMyTests(res.data)
        } 
        catch (err) {
           console.log(err); 
        }
        
    }, [])

    return !myTests ? <Loader type="Puff" color="#00BFFF" height={50} width={50} /> :
        myTests == 0 ?
            <div> לא שובצו לך מבחנים</div> :
            <>
                <div> יש מבחנים</div>
                {myTests.map(test =>
                    <Link key={test._id} to={`/test/${test._id}/examinee`}>
                        {test._id}
                        <br />
                        {test.status}
                    </Link>)
                }
            </>

}