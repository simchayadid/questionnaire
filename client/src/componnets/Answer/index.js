import axios from "axios"
import { useEffect, useState } from "react"
import './answer.css'

export default function Answer({ thisAnswer, quesId, index, status, onChange }) {
    const [answer, setAnswer] = useState(),
        // [isCorrect, setIsCorrect]=useState(),
        [color, setColor] = useState()
    useEffect(async () => {
        setAnswer(thisAnswer)
        //  setIsCorrect(thisAnswer.isCorrect)
        console.log(1111);
        thisAnswer.isCorrect ? setColor('green') : setColor('black')
    })
    console.log(answer)
    console.log();

    console.log(888);

    async function saveData(event) {
        event.preventDefault()
        // debugger
        let value
        if (event.target.type == "checkbox")
            value = event.target.checked
        else
            value = event.target.value
        const name = event.target.name
        const details = {
            index: index,
            quesId: quesId,
            name: name,
            value: value
        }
        console.log(details);
        try {
            // debugger
            const res = await axios.post('http://localhost:4000/updateAnswer', details)
            console.log(res.data);
            onChange(res.data)
            console.log(99);
        }
        catch (err) {
            console.log(err);
        }
    }
    // function www(){
    //     console.log(question);
    //     onChange(question)
    // }

    async function dealetAnswer(event) {
        event.preventDefault()
        const details = {
            index: index,
            quesId: quesId
        }
        console.log(details);
        try {
            const res = await axios.post('http://localhost:4000/dealetAnswer', details)
            console.log(res.data);
            // setAnswer()
            onChange(res.data)
        }
        catch (err) {
            console.log(err);
        }

    }

    return !answer ? <> </> :
        status == "Draft" ? <div>
            <input type="checkbox" style={{ paddingBottom: "1em" }} name="isCorrect" defaultChecked={answer.isCorrect} onBlur={saveData} ></input>
            <textarea type="text" name="text" placeholder="Answer " defaultValue={answer.text} onBlur={saveData} className='textarea-ans'></textarea>
            <i onClick={dealetAnswer} class="fas fa-times-circle tooltipp btn-color">
                <span class="tooltiptext">Delet Answer</span>
            </i>
        </div> :
            <div>
                {console.log(color)}
                {color == 'green' ? <i class="fas fa-check answer-icon green" ></i> : 
                <i class="fas fa-check answer-icon white"></i>}
                <b>{answer.text}</b>
            </div>

}