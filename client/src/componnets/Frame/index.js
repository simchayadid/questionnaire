import { useState, useContext } from "react";
import { Route, Switch } from 'react-router-dom'
import TestICreated from '../../pages/TestICreated'
import MyTests from '../../pages/MyTests'
import Test from '../../pages/Test'
import TestForm from '../../pages/TestForm'

// NavLink ייבוא של הקומפוננטה 
//אפשר לקבוע עיצוב מיוחד ללינק שבו נמצאים activeClassName שע"י התכונה Link ההבדל בינה לבין הקומפוננטה
import { NavLink } from "react-router-dom";
//כדי שנוכל להשתמש בו פה Login מהקומפוננטה userContext ייבוא של 
import { userContext } from '../../pages/Login'
import axios from "axios";
//ייבוא של העיצוב
import './header.css'






export default function Frame() {
    //userState הכנסת הקונטקסט לתוך המשתנה
    //User בגלל שהקונטקסט ששלחנו מכיל מערך עם מצב והגדרת מצב של  
    //אני קוראת לקונטקסט באותו שם בדיוק
    const userState = useContext(userContext),
        //מה שיתן לי את המשתמש שמחובר עכשיו userState שיכיל את האיבר הראשון במערך user הגדרת משתנה בשם 
        // והגדרת משתנה שני שיכיל את המשתנה השני במערך שהוא הפונקציה שמגדירה את המצב
        [user, setUser] = userState,
        [message, setMessage] = useState()
    // [show, setShow] = useState(false)


    //כשהמשתמש מתנתק
    async function logOut() {
        try {
            const token = sessionStorage.token ? sessionStorage.token : localStorage.token
            console.log(token);
            const res = await axios.post('http://localhost:4000/logout', { token })
            console.log(res.data);
            //מוחקת את התוקן
            delete localStorage.token
            delete sessionStorage.token
            // מאפסת את היוזר
            setUser()
            setMessage(res.data)

        }
        catch (err) {
            console.log(err);
            setMessage(err.response?.data?.error || err.message)
        }
    }
    //פתיחה וסגירה של פרופיל
    function showInfo(event) {
        console.log(event.target.parentElement.classList);
        event.target.parentElement.classList.toggle("show")
        DropdownInfo.classList.toggle("show")
    }
    //פתיחה וסגירה של הצד
    function closeSidebar() {
        accordionSidebar.classList.toggle('toggled')
    }



    return <div id="page-top">

        {/* <!-- Page Wrapper --> */}
        <div id="wrapper">

            {/* <!-- Sidebar --> */}
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

                {/* <!-- Sidebar - Brand --> */}
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <i class="fas fa-laugh-wink"></i>
                    </div>
                    <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
                </a>

                <hr class="sidebar-divider my-0" />

                {/* <!-- Nav Item - Dashboard --> */}
                <li class="nav-item">
                    <a class="nav-link" href="index.html">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span></a>

                </li>
                <nav>
                    <NavLink activeClassName='active' class=' nav-link' to="/" exact>tests i created</NavLink>
                    <hr class="sidebar-divider" />
                    <NavLink className='center' activeClassName='active' to="/my-tests">  tests </NavLink>
                </nav>

                <hr class="sidebar-divider d-none d-md-block" />
                <div class="text-center d-none d-md-inline">
                    <button onClick={closeSidebar} class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>
            </ul>
            {/* <!-- End of Sidebar --> */}

            {/* <!-- Content Wrapper --> */}
            <div id="content-wrapper" class="d-flex flex-column">

                {/* <!-- Main Content --> */}
                <div id="content">
                   
                        {/* <!-- Topbar --> */}
                        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                            {/* <!-- Sidebar Toggle (Topbar) --> */}
                            <button onClick={closeSidebar} id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                                <i class="fa fa-bars"></i>
                            </button>



                            {/* <!-- Topbar Navbar --> */}
                            <ul class="navbar-nav ml-auto">

                                {/* <!-- Nav Item - Search Dropdown (Visible Only XS) --> */}
                                <li class="nav-item dropdown no-arrow d-sm-none">
                                    <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-search fa-fw"></i>
                                    </a>

                                </li>




                                <div class="topbar-divider d-none d-sm-block"></div>
                                <header>

                                    <div style={{ color: "red" }}>{message}</div>
                                </header>
                                <div className="my-auto">
                                    hello {user.name}</div>

                                <div class="topbar-divider d-none d-sm-block"></div>
                                {/* <!-- Nav Item - User Information --> */}
                                <li class="nav-item dropdown no-arrow ">
                                    {/* --------------------- */}
                                    <a class="nav-link dropdown-toggle" onClick={showInfo} id="userDropdown" role="button"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {/* /////////////////////////////////// */}
                                        <img class=" pro-image img-profile rounded-circle " src={require("../../img/undraw_profile_3.jpg")} />
                                    </a>

                                    {/* <!-- Dropdown - User Information --> */}
                                    <div id='DropdownInfo' class="dropdown-menu dropdown-menu-right shadow animated--grow-in "
                                        aria-labelledby="userDropdown">
                                        <a class="dropdown-item" href="#">
                                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Profile
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Settings
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Activity Log
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" onClick={logOut} data-toggle="modal" data-target="#logoutModal">
                                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                            Logout
                                        </a>
                                    </div>
                                </li>

                            </ul>

                        </nav>
                       
                           
                       
                    
                    {/* <!-- End of Topbar --> */}

                    {/* <!-- Begin Page Content --> */}
                    <div class="container-fluid col-lg-8 mb-3 mb-sm-0">

                        <Switch>
                            {/* / -אומרת שתיגש לקובץ הזה בדיוק כי אם לא הוא ילך לכל כתובת שמתחילה ב exact התכונה */}
                            <Route path="/" component={TestICreated} exact />
                            <Route path="/my-tests" component={MyTests} />
                            <Route path="/test/:testId/:asks" component={Test} />
                            {/* בא לומר שהפרמטר רשות testId סימן השאלה אחרי  */}
                            <Route path="/test-form/:testId?/:status?" component={TestForm} />
                        </Switch>


                    </div>

                </div>

                {/* <!-- Footer --> */}
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span> &copy; Simcha Yadid 2021</span>
                        </div>
                    </div>
                </footer>
                {/* <!-- End of Footer --> */}

            </div>
            {/* <!-- End of Content Wrapper --> */}

        </div>
        {/* <!-- End of Page Wrapper --> */}

    </div >





}
