import axios from "axios"
import { useEffect, useState, createContext } from "react"
import Loader from "react-loader-spinner";
import { Link } from "react-router-dom";
import MessagePopUp from "../MessagePopUp";

export const popUpContext = createContext()

export default function Placement({ testId }) {
    console.log(testId);
    const [testPlac, setTestPlac] = useState(),
        [popUp, setPopUp] = useState(),
        popUpState =[popUp, setPopUp,testId]

    useEffect(async function () {
        try {

            const res = await axios.get(`http://localhost:4000/getTestsOfId/${testId}`)
            console.log(res.data);
            setTestPlac(res.data)


        }
        catch (err) {
            console.log(err);
        }
    }, [])

    async function changeState(){
        setPopUp(true)
    }
    return <popUpContext.Provider value={popUpState}>

        <button onClick={changeState}> Assign a test</button>
        <MessagePopUp />
        {!testPlac ? <Loader type="Puff" color="#00BFFF" height={50} width={50} /> :
            testPlac.length != 0 ?
                <div>
                  {  testPlac.map(test =>
                    <Link key={test._id} to={`/test/${test._id}/tester`} >
                        <div style={{border:"1px solid black"}}>
                            <label><u>Examinee Email:</u> {test.examineeEmail} </label>
                            <label><u>status:</u> {test.status} </label>

                            
                            </div>
                           </Link> )}
                            
                    
                </div> :
                <div> no placement yet</div>}
    </popUpContext.Provider>

}