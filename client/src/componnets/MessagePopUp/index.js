import axios from "axios"
import { popUpContext } from '../Placement'
import { useContext } from "react"

export default function MessagePopUp() {
    const popUpState = useContext(popUpContext),
        [popUp, setPopUp, testId] = popUpState
    console.log(popUp, testId);


    async function sandEmail(event) {
        event.preventDefault()

        const values = Object.values(event.target)
            .reduce((acc, input) => !input.name ? acc : ({
                ...acc,
                [input.name]: input.type == 'checkbox' ? input.checked : input.value
            }), {}
            )
        values.testId = testId
        console.log(values)

        const res = await axios.post('http://localhost:4000/assignAndSandTest', values)
        console.log(res.data);

        setPopUp(false)
    }

    return popUp ? <div>
        <form onSubmit={sandEmail}>
            <label>to:</label>
            <input type="email" name="email" required multiple></input>
            <label>subject:</label>
            <input type='text' name="subject" required></input>
            <label>message:</label>
            <input type='text' name="message" required></input>
            <input type='submit'></input>
        </form>
    </div> : <></>
}