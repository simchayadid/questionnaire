import axios from "axios"
import { useState } from "react"
import AnswerPlac from "../AnswerPlac"


export default function QuestionPlac({ ques, index,status }) {
    const [question, setQuestion] = useState(ques),
        [answers, setAnswers] = useState(ques.answers)


    return status== 'Assigned'?
    <div>
        <h1>:שאלה מספר {index+1}</h1>
        <h4>{question.context}</h4>
        {
            answers.map(ans =>
                <AnswerPlac key={ans._id} answer={ans} quesId={question._id} status={status}/>
            )
        }
    </div>:
    <div>
    <h1>:שאלה מספר {index+1}</h1>
    <h4>{question.context}</h4>
    {
        answers.map(ans =>
            <AnswerPlac key={ans._id} answer={ans} quesId={question._id} status={status}/>
        )
    }
</div>

}