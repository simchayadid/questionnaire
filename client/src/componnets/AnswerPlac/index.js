import axios from "axios"
import { useContext, useState, useEffect } from "react"
import { testContext } from "../../pages/Test"

export default function AnswerPlac({ answer, quesId ,status}) {

    const testId = useContext(testContext),
        [isChecked, setIsChecked] = useState()
    console.log(testId)

    useEffect(async () => {
        console.log(4444);
        const data = {
            testId: testId,
            quesId: quesId,
            answerId: answer._id
        }
        console.log(data);
        try {
            const res = await axios.post(`http://localhost:4000/isAnswerChecked`, data)
        console.log(res.data+"  "+answer._id);
        setIsChecked(res.data)
        } 
        catch (err) {
            console.log(err);
        }
        
    }, [])
    async function saveData(event) {
        // debugger
        // event.preventDefault();
        console.log(answer.text);
        let data = {
            test: testId,
            quesId: quesId,
            answer: answer._id,
            state: event.target.checked
        }
        console.log(data)
        // debugger
        try {
        await axios.post('http://localhost:4000/updateAnswerPlac', data)
        } 
        catch (err) {
           debugger
           console.log(err); 
        }
       
    }

    return  status== 'Assigned'?
    <div>{console.log(isChecked)}
        <label>{answer.text}</label>
        <input type="checkbox" onClick={saveData} defaultChecked={isChecked}></input>
    </div> :
    <div>{console.log(isChecked)}
    <label>{answer.text}</label>
</div>
}