import axios from "axios";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import Question from "../../componnets/Question";
import Loader from "react-loader-spinner";



export default function Form({ testId }) {
    const
        [test, setTest] = useState(),
        [questions, setQuestions] = useState(''),
        [load, setLoad] = useState(0),
        [deadline, setDeadline] = useState(),
        [error, setError] = useState(''),
        [error1, setError1] = useState('')


    let history = useHistory()

    console.log(deadline);
    console.log(testId);

    // משתנה testId -פונקציה שמבוצעת בכל פעם ש
    useEffect(async () => {
        console.log("נכנסתי לדף");
        // debugger
        //(רוצה לערוך אותו) זה אומר שהמשתמש לחץ על מבחן שיצר כבר testId אם יש 
        if (testId) {
            try {
                console.log("נכנסתי לדף");
                const res = await axios.get(`http://localhost:4000/getTest/${testId}`)
                setTest(res.data[0]);
                setQuestions(res.data[1])
                let time = res.data[0].deadline
                time = time.slice(0, 10)
                setDeadline(time);
                setError1('')
                // debugger
            }
            catch (err) {
                debugger
                setError(err?.response?.data?.error || err.message)
            }
        }

    }, [testId, load])

    //פונקציה שמבוצעת כאשר משתמש לוחץ על צור מבחן
    async function creatTest(event) {
        debugger
        event.preventDefault()
        //קבלת התוקן מהלוקאל סטורג
        var token = sessionStorage.token ? sessionStorage.token : localStorage.token
        const value = event.target.firstElementChild.value

        // debugger
        const data = {
            token: token,
            name: value
        }

        try {
            const res = await axios.post('http://localhost:4000/creatTest', data)
            console.log(res.data);
            history.push(`/test-form/${res.data}`)
        }
        catch (err) {
            console.log(err);
            err.response.data.erroe
            setError(err.response?.data?.erroe?.message || err.message)

        }

    }


    async function saveData(event) {
        // debugger
        const value = event.target.value
        const name = event.target.name
        const details = {
            testId: test._id,
            name: name,
            value: value
        }
        console.log(details);
        try {
            const res = await axios.post('http://localhost:4000/updateTest', details)
            console.log(res.data);
        }
        catch (err) {
            console.log(err);
        }

    }

    async function addQuestion(event) {
        event.preventDefault()
        //קבלת התוקן מהלוקאל סטורג
        var token = sessionStorage.token ? sessionStorage.token : localStorage.token
        const data = {
            token: token,
            testId: testId
        }
        try {
            const res = await axios.post('http://localhost:4000/creatQues', data)
            // console.log(res.data);
            // history.push(`/test-form/${res.data}`)
            setLoad(load + 1)
        }
        catch (err) {

            console.log(err)
            setError(err.response?.data?.error || err.message)

        }
    }

    async function saveTest() {
        try {
            console.log("save test")
            console.log(questions.length);
            // debugger

            const res = await axios.post('http://localhost:4000/saveTest', { testId })
            console.log(res.data);
            setLoad(load + 1)
        }
        catch (err) {
            debugger
            console.log(err);
            setError1(err.response?.data?.error || err.message)
        }
    }

    // אם לא קיבלתי מבחן בפראמס, זה אומר שהמשתמש לחץ "הוסף מבחן" אז מראה טופס של יצירת מבחן חדש
    return !testId ?
        <div>
            <h1>יצירת מבחן</h1>
            <form onSubmit={creatTest}>
                <input type="text" name="name" placeholder="test name" autoFocus /><br />
                <div style={{ color: 'red' }} >{error}</div>
                <input type="submit" value="צור מבחן"></input>
            </form>
        </div> :
        error ?
            <div style={{ color: "red" }}>{error}</div> :
            //Loader אם לא חזרה תשובה מהשרת מציגה את
            !test || !questions ?
                <div className="flex-box">
                    < Loader type="Circles" color="#00BFFF" height={50} width={50} />
                </div> :
                //אם המבחן טיוטא
                test.status == 'Draft' ?
                    <div>
                        <div class="main card shadow mb-4 left-margin border-focus">
                            <div class="card-body">
                                <form>
                                    <textarea type="text" name="name" className='h3 ' defaultValue={test.name} onBlur={saveData} placeholder="Test Name"></textarea>
                                    <textarea type="text" name="description" className='h5 ' defaultValue={test.description} placeholder="description" onBlur={saveData} placeholder="Test Description"></textarea>
                                    <label className="tooltipp" >
                                        <input type="date" className="deadline" name="deadline" defaultValue={deadline} onBlur={saveData} />
                                        <span class="tooltiptext">Deadline</span>
                                    </label>
                                    <div style={{ color: 'red' }}> {error}</div>

                                </form>
                            </div>
                        </div>
                        {
                            questions.map((q, i) =>
                                <Question key={q._id} ques={q} index={i + 1} status={test.status} />)
                        }
                        <div style={{ color: "red" }}> {error1}</div>
                        <div className="save-div">
                            <button onClick={saveTest} class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                                <i class="fas fa-save fa-lg text-white-50 save-i"></i>save test
                            </button>
                        </div>

                        <div class="scroll-to-top rounded ">
                            <i onClick={addQuestion} class="fas fa-plus-circle tooltip1 btn-color" >
                                <span class="tooltiptext1">Add Question</span>
                            </i>
                            <a href="#page-top">
                                <i class="fas fa-chevron-circle-up tooltip1 btn-color ">
                                    <span class="tooltiptext1">Top Of Page</span>
                                </i>
                            </a>
                        </div>
                    </div> :
                    //אם המבחן נשמר
                    <div>
                        <div class="main card shadow mb-4 left-margin border-focus">
                            <div class="card-body ">
                                <h1>test : {test.name} </h1>
                                <h3>description : {test.description}</h3>
                                <h4>deadline : {deadline}</h4>
                            </div>
                        </div>
                        {
                            questions.map((q, i) =>
                                <Question key={q._id} ques={q} index={i + 1} status={test.status} />

                            )
                        } 
                        <div class="scroll-to-top rounded " style={{height:'3em'}} >
                            <a href="#page-top">
                                <i class="fas fa-chevron-circle-up tooltip1 btn-color ">
                                    <span class="tooltiptext1">Top Of Page</span>
                                </i>
                            </a>
                        </div>
                    </div >
}





