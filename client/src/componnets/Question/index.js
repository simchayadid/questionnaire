import axios from "axios";
import { useState, useContext } from "react"
import Answer from "../Answer";
import './question.css'
export default function Question({ ques, index, status }) {
    // debugger


    const [question, setQuestion] = useState(ques),
        [error, setError] = useState(),
        [changh, setChange] = useState(1)
    console.log(question);
    const answers = question.answers
    console.log(answers);
    console.log(changh);

    function handleChange(question) {
        console.log(55555);
        console.log(question);
        setQuestion(question);
    }
    async function saveData(event) {
        event.preventDefault()
        const value = event.target.value
        const name = event.target.name
        const details = {
            quesId: question._id,
            name: name,
            value: value
        }
        console.log(details);
        try {
            const res = await axios.post('http://localhost:4000/updateQues', details)
            console.log(res.data);
        }
        catch (err) {
            console.log(err);
        }

    }

    async function addAnswer(event) {
        event.preventDefault()
        const questionId = question._id
        try {
            const res = await axios.post('http://localhost:4000/addAnswer', { questionId })
            setQuestion(res.data)
        }
        catch (err) {
            console.log(err);
        }

    }
    async function dealetQuestion(event) {
        event.preventDefault()
        const details = {
            quesId: question._id,
            name: "active",
            value: false
        }
        // debugger
        console.log(details);
        try {
            const res = await axios.post('http://localhost:4000/updateQues', details)
            console.log(res.data);
            setQuestion('')
        }
        catch (err) {
            console.log(err);
            setError(err.response?.data?.error || err.message)
        }

    }
    function markDiv(event) {
        // debugger
        console.log(9999);
        event.target.parentElement.parentElement.parentElement.classList.toggle("border-focus")
    }


    return !question ? '' :
        //במקרה שקיימת שאלה
        <>
            <div class="card shadow mb-4 left-margin border-focus" >
                < div class="card-header py-3 context dataTables_length" >
                    {
                        status == "Draft" ?
                            <>
                                <textarea type="text" name="context" placeholder="שאלה" defaultValue={question.context + '?'} onBlur={saveData} />
                                <div >
                                    <label className=" tooltipp">
                                        <input type="number" name="points" min="1" max="100" className=" form-control form-control-sm points-input tooltipp" defaultValue={question.points} onBlur={saveData} />
                                        <span class="tooltiptext">Question Points</span>

                                    </label>

                                    <i onClick={dealetQuestion} class="far fa-trash-alt del-butten form-control-sm tooltipp">
                                        <span class="tooltiptext">Delet Question</span>
                                    </i>
                                </div>
                                <div style={{ color: "red" }}> {error}</div>
                            </>
                            :
                            <>
                                <h4 class="m-0 font-weight-bold text-primary">
                                    {question.context}?

                                </h4>
                                <h6>({question.points} points)</h6>
                            </>
                    }
                </div>
                < div class="card-body" >
                    {
                        answers.map((ans, i) => {
                            return <Answer key={Math.random()} thisAnswer={ans} quesId={question._id} index={i} status={status} onChange={handleChange}></Answer>
                        })
                    }
                    {
                        status == "Draft" ?
                            <><i onClick={addAnswer} class="fas fa-plus-square btn-color tooltipp fa-lg">
                                <span class="tooltiptext">Add Answer</span></i>
                            </>
                            : ''
                    }
                </div >
            </div >
            {/* תשובות */}





        </>
}
